# fish-colocalization

Python package for colocalization of [RS-FISH](https://github.com/PreibischLab/RS-FISH/tree/master) results with either RS-FISH results (e.g from two image channels) 2D/3D, or with an image (only 2D for now).

## Installation
Create a conda env using environment.yml.yml, activate the environment, and install the python package (e.g. clone the repo and run setup.py). 

## Usage  

fish-colocalization -i <path/to/input/directory> --pattern1 <wildcard pattern> --pattern2 <wildcard pattern>  

## Arguments

-i, --input_path, required=True  
Input directory path where all the input csvs (or csvs and images) are. 

`-p1, --pattern1, required=True`  
File name pattern of the first group of files (either images or csv). e.g. `\*.tif`  
If input pattern here corresponds to image files, either 2D or 3D images are accepted, but in case of 3D images the argument `image-channel` must be provided as for now only 2D images are supported.

`-p2, --pattern2, required=True`  
File name pattern of the first group of files (csv only). e.g. `C1-\*.csv`  

`-d, --min_dist, type=int, default=2`   
Maximal distance between detected points to be considered colocalized. Only relevant for csv-csv colocalization.   

`-c, --image_channel, type=int, default=-1`  
Which image channel to use in the input images. Only relevant for img-csv colocalization and will only be used if input images are 3D.  

`-t, --threshold_method,  default=otsu`   
Methos for thresholding the input image to become a binary image. Only relevant for img-csv colocalization.  


## Authors

Schmidt Lab of Helmholz Imaging  
ella.bahry@mdc-berlin.de  

## License

The MIT License

