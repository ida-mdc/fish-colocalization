from distutils.core import setup

setup(name="fish_colocalization",
version="1.0.0",
description="colocalize RSFISH detection - either csv to csv (2D/3D), or csv to image (channel - currently only 2D)",
author="Ella Bahry",
author_email='ella.bahry@mdc-berlin.de',
url="https://gitlab.com/ida-mdc/fish-colocalization",
packages=["fish_colocalization"],
)
