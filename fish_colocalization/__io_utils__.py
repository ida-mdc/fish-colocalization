from datetime import datetime
import os
from glob import glob
import pandas as pd
import numpy as np
import logging
import re
import tifffile as tif


def create_result_dir(path):
    now = datetime.now()
    timestamp_str = now.strftime("%Y%m%d_%H%M%S")
    dir_name = f'colocalization_results_{timestamp_str}'
    result_path = os.path.join(path, dir_name)
    os.makedirs(result_path)

    return result_path


def save_args_to_file(args, result_path):
    with open(os.path.join(result_path, 'arguments.txt'), 'w') as file:
        for arg in vars(args):
            file.write(f'{arg}: {getattr(args, arg)}\n')


def get_paths_lists(input_path, pattern1, pattern2):

    paths1 = sorted(glob(os.path.join(input_path, pattern1)))
    paths1.extend(sorted(glob(os.path.join(input_path, '*', pattern1))))
    logging.info(f'Number of files group1: {len(paths1)}')

    paths2 = sorted(glob(os.path.join(input_path, pattern2)))
    paths2.extend(sorted(glob(os.path.join(input_path, '*', pattern2))))
    logging.info(f'Number of csv group2: {len(paths2)}')

    if len(paths1) != len(paths2):
        raise ValueError("The lengths of the two file path lists do not match.")

    logging.info('PLEASE REVIEW THE PAIRS OF FILES TO COLOCALIZE:')
    for item1, item2 in zip(paths1, paths2):
        logging.info(f"Pair:\n{os.path.basename(item1)},\n{os.path.basename(item2)}")


    #paths1, paths2 = sort_lists_to_match_item_pairs(paths1, paths2)

    return paths1, paths2


def extract_number_from_str(path):
    """Extracts the numeric part from the filename before the extension."""
    # Not using. Need a better way of matching files.
    match = re.search(r'(\d+)(?=\.\w+$)', path)
    return int(match.group()) if match else None


def sort_lists_to_match_item_pairs(paths1, paths2):
    # Extract numbers from both lists
    # Not using. Need a better way of matching files.
    numbers1 = {extract_number_from_str(f): f for f in paths1}
    numbers2 = {extract_number_from_str(f): f for f in paths2}

    # Find common keys and remove non-matching items
    common_keys = numbers1.keys() & numbers2.keys()
    paths1[:] = [numbers1[key] for key in sorted(common_keys)]
    paths2[:] = [numbers2[key] for key in sorted(common_keys)]

    # Check for missing items and print messages
    for number in set(numbers1.keys()) - common_keys:
        logging.warning(f"{number:02d} missing from list2, thus deleted from list1")
    for number in set(numbers2.keys()) - common_keys:
        logging.warning(f"{number:02d} missing from list1, thus deleted from list2")

    logging.info(f'Updated path1:, {paths1}')
    logging.info(f'Updated path2:, {paths2}')

    return paths1, paths2


def read_img(path, channel):
    img = tif.imread(path)

    if img.ndim == 2:
        if channel != -1:
            logging.warning(f'Channel was given, but input image has only 2D. Using original image as is.')
    elif img.ndim == 3:
        if channel != -1:
            logging.warning(f'Skipping image as image is 3D and channel number was not given.')
            return None

        channel_dim = np.argmin(img.shape)
        if channel_dim == 2:
            img = img[:, :, channel]
        else:
            img = img[channel, :, :]
    else:
        logging.warning(f'Skipping image as image has more than 3 dimensions.')
        return None

    return img


def read_fish_spots(csv_path):
    try:
        df = pd.read_csv(csv_path)
    except (FileNotFoundError, IndexError):
        logging.warning(f'Skipping image as csv is missing.')
        return None

    spots_arr = df[["y", "x", "z"]].to_numpy() if "z" in df.columns else df[["y", "x"]].to_numpy()

    return spots_arr
