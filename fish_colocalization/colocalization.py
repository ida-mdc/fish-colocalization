import os
import pandas as pd
from skimage import filters
from scipy.ndimage import distance_transform_edt
import numpy as np
import logging
import copy
from scipy import spatial
from fish_colocalization import __io_utils__ as io_utils


def set_logger(result_path):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Create handlers
    file_handler = logging.FileHandler(os.path.join(result_path, 'logfile.log'))  # Log to a file
    console_handler = logging.StreamHandler()  # Log to the console

    # Create a formatter and set it for both handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)

    # Add handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    logging.info("Starting")


def colocalize(args):

    result_path = io_utils.create_result_dir(args.input_path)

    set_logger(result_path)
    io_utils.save_args_to_file(args, result_path)

    paths1, paths2 = io_utils.get_paths_lists(args.input_path, args.pattern1, args.pattern2)

    if paths1[0][-4:] == '.csv':
        colocalize_csv_to_csv(paths1, paths2, args.min_dist, result_path)
    else:
        colocalize_image_to_csv(paths1, paths2, args.image_channel, args.threshold_method, result_path)


def colocalize_image_to_csv(paths1, paths2, image_channel, threshold_method, result_path):
    df = pd.DataFrame(columns=['image_index', 'image_name', 'distance'])

    for ip, p in enumerate(paths1):

        logging.info(f'Processing image {ip} out of {len(paths1)}. Name {os.path.basename(p)}')

        img = io_utils.read_img(p, image_channel)
        if img is None:
            continue

        thr_img = get_threshold_img(img, threshold_method)
        dist_map = get_distance_map(thr_img)

        spots = io_utils.read_fish_spots(paths2[ip])
        if spots is None:
            continue
        elif spots.shape[1] == 3:
            logging.warning('For image-csv colocalization expected csv without z column, but z column was found.')

        distances = get_spots_distances(spots, dist_map)

        data_to_add = pd.DataFrame({'image_index': ip,
                                    'image_name': os.path.basename(p),
                                    'distance': distances})

        df = pd.concat([df, data_to_add], ignore_index=True)

    df.to_csv(os.path.join(result_path, f'distances.csv'))  # , index=False)


def colocalize_csv_to_csv(paths1, paths2, min_dist, result_path):

    df = pd.DataFrame(columns=['image_name',
                               'n_points_1',
                               'n_points_2',
                               'n_matches',
                               'mean_euc_dist'])

    for ip, p in enumerate(paths1):

        logging.info(f'Processing image {ip} out of {len(paths1)}. Name {os.path.basename(p)}')

        spots1 = io_utils.read_fish_spots(p)
        if spots1 is None:
            continue
        spots2 = io_utils.read_fish_spots(paths2[ip])
        if spots2 is None:
            continue

        match_spots1, match_spots2, distances, mean_euc_dist = compare_spot_sets(spots1, spots2, min_dist)

        df.loc[ip] = [os.path.basename(p),
                      len(spots1),
                      len(spots2),
                      len(distances),
                      round(mean_euc_dist, 4)]

        match_spots1 = [','.join(map(str, sublist)) for sublist in match_spots1]
        match_spots2 = [','.join(map(str, sublist)) for sublist in match_spots2]

        df_img = pd.DataFrame({'match_point_1': match_spots1,
                               'match_point_2': match_spots2,
                               'distance': distances})

        df_img.to_csv(os.path.join(result_path, f'{ip}.csv'))

    df.to_csv(os.path.join(result_path, f'summary.csv'))


def get_threshold_img(img, method='otsu'):
    flat_image = img.flatten()

    if method == 'li':
        thr = filters.threshold_li(flat_image)
    elif method == "otsu":
        thr = filters.threshold_otsu(flat_image)
    elif method == "triangle":
        thr = filters.threshold_triangle(flat_image)
    elif method == 'yen':
        thr = filters.threshold_yen(flat_image)
    else:
        raise ValueError(f'Unknown threshold method: {method}')

    threshold_img = img < thr

    if np.sum(threshold_img) < threshold_img.size // 2:
        logging.warning('Threshold image has more pixels of object than background.'
                        'Please check that the thresholding did not reverse the background and object values')

    return threshold_img


def get_spots_distances(spots, dist_map):

    distances = []

    for x, y in spots:
        x1, y1 = int(x), int(y)
        x2, y2 = int(np.ceil(x)), int(np.ceil(y))

        # 4 pixels involved in the interpolation
        q11 = dist_map[x1, y1]
        q12 = dist_map[x1, y2]
        q21 = dist_map[x2, y1]
        q22 = dist_map[x2, y2]

        # Bi-linear interpolation formula
        interpolated_value = (q11 * (x2 - x) * (y2 - y) +
                              q21 * (x - x1) * (y2 - y) +
                              q12 * (x2 - x) * (y - y1) +
                              q22 * (x - x1) * (y - y1))

        distances.append(interpolated_value)

    return distances


def get_distance_map(bw_img):
    dist_map = distance_transform_edt(bw_img)
    return dist_map


def invert_image_values(bw_img):
    img_inv = np.logical_not(bw_img)
    return img_inv


def compare_spot_sets(set1, set2, min_dist):
    distances = []

    removed_items = True
    euc_dist = 0

    # for spot identity analysis
    used_spots_gt = []
    used_spots_detected = []

    while removed_items and len(set2) != 0 and len(set1) != 0:

        min_dist_curr = 10000
        min_index_set1 = -1
        min_index_set2 = -1
        counter = 0
        kd_copy = copy.deepcopy(set2)
        kdtree = spatial.KDTree(kd_copy)

        for item in set1:
            distance, index = kdtree.query(item)  # a new KD tree is made

            if distance < min_dist_curr:
                min_dist_curr = distance
                min_index_set1 = counter
                min_index_set2 = index

            counter = counter + 1

        if min_dist_curr < min_dist:  # if less than min dist

            # for spot identity analysis
            used_spots_gt.append(set1[min_index_set1])
            used_spots_detected.append(set2[min_index_set2])

            set2 = np.delete(set2, min_index_set2, axis=0)  # delete mod ind
            set1 = np.delete(set1, min_index_set1, axis=0)  # delete spots_set1 ind
            removed_items = True
            distances.append(min_dist_curr)

        else:
            removed_items = False
    if len(distances) > 0:
        distances = np.around(np.asarray(distances), 4)
        euc_dist = np.mean(distances)

    return np.asarray(used_spots_gt), np.asarray(used_spots_detected), distances, euc_dist
