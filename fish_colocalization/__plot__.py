import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker


def plot_and_save_histogram(pixel_distances, plot_path):
    bin_size = 0.1

    conversion_factor = 4.615  # a micrometer is 4.615 pixels
    mm_distances = [dist / conversion_factor for dist in pixel_distances]

    # Create histogram without density=True, capturing the returned values
    counts, bin_edges = np.histogram(mm_distances, bins=np.arange(min(mm_distances),
                                                                  max(mm_distances) + bin_size,
                                                                  bin_size))

    # Calculate percentage for each bin
    counts_percentage = counts / sum(counts)

    # Plot the histogram using plt.bar instead of plt.hist
    plt.bar(bin_edges[:-1], counts_percentage, align='center', width=bin_size)

    # Set the x-ticks to be at the middle of the bins
    plt.xticks(bin_edges[:-1], rotation=45)

    # Format y-axis as percentage
    plt.gca().yaxis.set_major_formatter(ticker.PercentFormatter(1.0))

    # Label the axes
    plt.xlabel('Distance (μm)')
    plt.ylabel('Percentage')  # ('Count')

    title = f'Distance of FISH spots from object'
    plt.title(title)

    plt.savefig(plot_path, bbox_inches='tight')
    # Show the plot in the Jupyter notebook
    # plt.show()
    plt.clf()
