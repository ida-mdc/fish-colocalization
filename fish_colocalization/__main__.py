import argparse
from fish_colocalization import colocalization as colocalization


def get_user_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input_path', required=True)
    parser.add_argument('-p1', '--pattern1', required=True)
    parser.add_argument('-p2', '--pattern2', required=True)
    parser.add_argument('-d', '--min_dist', default=2, type=int)
    parser.add_argument('-c', '--image_channel', default=-1, type=int)
    parser.add_argument('-t', '--threshold_method', default='otsu')

    return parser.parse_args()


def main():
    args = get_user_arguments()
    colocalization.colocalize(args)


if __name__ == '__main__':
    main()
